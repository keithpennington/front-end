<!DOCTYPE html>
<html>
<head>
<script src="https://use.fontawesome.com/356c784d4c.js"></script>
<link href="css/style.min.css" media="all" rel="stylesheet" />
<title>Check Out | Generation Genius</title>
</head>

<body>
<main id="gen-main">
<div class="gen-checkout-wrapper">
<div class="checkout-top">
<h1>Checkout</h1>
</div>
<div class="checkout-mid">
  <form method="post" action="/checkout-complete.php">
    <div class="checkout-cols-wrapper">
      <div class="col">
        <h2>Create Login</h2>
        <p class="description">
          Teachers & students share one login
        </p>
        <fieldset>
          <input type="text" id="username" name="username" placeholder="Username" />
          <input type="password" id="password" name="password" placeholder="Enter Your Password" />
          <input type="password" id="pin" name="pin" placeholder="Create a 4-digit Pin" />
          <span class="input-desc">PIN is for access to the manage account page.</span>
        </fieldset>
      </div>
      <div class="col">
        <h2>Customer Information</h2>
        <p class="description">
          For the person/organization paying
        </p>
        <fieldset>
          <input type="text" id="your_name" name="your_name" placeholder="Your Name" />
          <select id="title" name="title">
            <option value="" disabled selected>Title</option>
            <option value="teacher">Teacher</option>
            <option value="librarian">Librarian</option>
            <option value="admin">Administrator</option>
          </select>
          <input type="email" id="email" name="email" placeholder="Your Email" />
          <input type="text" id="school_name" name="school_name" placeholder="School Name" />
          <input type="text" id="billing_addr" name="billing_addr" placeholder="Billing Address" />
          <input type="text" id="city" name="city" placeholder="City" />
          <select id="state" name="state">
            <option value="" disabled selected>State</option>
            <option value="il">IL</option>
            <option value="in">IN</option>
            <option value="oh">OH</option>
          </select>
          <input type="text" id="zip" name="zip" placeholder="Zip" />
          
        </fieldset>
        <a href="#" class="gen_link">Click here for international address.</a>
      </div>
      <div class="col">
        <h2>Secure Payment</h2>
        <p class="description">
          Choose payment method below
        </p>
        <fieldset>
          <input type="radio" id="cc" name="payment_type" value="cc" />
          <label for="cc">Credit/Debit Card</label>
          <input type="radio" id="po" name="payment_type" value="po" />
          <label for="po">Purchase Order</label>
          <input type="text" id="cc-number" name="cc-number" placeholder="Card Number" />
          <input type="text" id="exp_month" name="exp_month" placeholder="MM" />
          <input type="text" id="exp_year" name="exp_year" placeholder="YY" />
          <input type="password" id="cvc" name="cvc" placeholder="CVC" />
        </fieldset>
        <div class="submit-wrapper">
        <i class="fa fa-lock" aria-hidden="true" style="align-self: center; font-size: 20px; flex-basis: 40%; text-align: right;"></i>
          <input type="submit" id="submit" style="font-size: 20px;" value="Pay Now" />
          <i class="fa fa-arrow-circle-right" aria-hidden="true" style="align-self: center; font-size: 40px; flex-basis: 40%; text-align: right;"></i>
        </div>
      </div>
      <div class="col summary">
        <div class="summary_top">
          <h2>Order Summary</h2>
          <h3 style="font-size: 16px" >School Yearly Plan</h3>
          <p>
          This is a yearly recurring subscription paid once a year at $795. Cancel any time with 1 click on the manage account page. We send email reminders 30 days before each renewal.
          </p>
          <select id="sub_plan" name="sub_plan">
            <option value="one_year">1 Year @ $795 per year</option>
            <option value="one_year">2 Year @ $695 per year</option>
          </select>
          <div class="total">
            <label>Total</label>
            <div class="amount"><strong>$795</strong></div>
          </div>
        </div>
          <div class="summary_bottom">
          <i class="fa fa-bell" style="color: #f48d1d; font-size: 24px;"></i>
          <div class="promo_text">
            <h4 style="color: #ffffff; margin: 0;" >January Promotion Applied</h4>
              <p style="color: #deadde; margin: 0;">
                9 Months Free Will Be Auto-Added To Plan
              </p>
          </div>
    
        </div>
      </div>
    </div>
  </form>

</div>
</div>
</main>
</body>

</html>